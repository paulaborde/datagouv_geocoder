# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main2.ui'
#
# Created: Fri Dec  2 11:35:54 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(328, 600)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("searchico.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.groupBox_4 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_4.setGeometry(QtCore.QRect(0, 280, 321, 141))
        self.groupBox_4.setObjectName(_fromUtf8("groupBox_4"))
        self.cb_getheader = QtGui.QComboBox(self.groupBox_4)
        self.cb_getheader.setGeometry(QtCore.QRect(60, 60, 201, 22))
        self.cb_getheader.setObjectName(_fromUtf8("cb_getheader"))
        self.groupBox_3 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(0, 420, 321, 141))
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.label_results = QtGui.QLabel(self.groupBox_3)
        self.label_results.setGeometry(QtCore.QRect(10, 70, 301, 16))
        self.label_results.setAlignment(QtCore.Qt.AlignCenter)
        self.label_results.setObjectName(_fromUtf8("label_results"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(0, 0, 321, 141))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.btn_getpath = QtGui.QPushButton(self.groupBox)
        self.btn_getpath.setGeometry(QtCore.QRect(110, 40, 91, 23))
        self.btn_getpath.setObjectName(_fromUtf8("btn_getpath"))
        self.label_path = QtGui.QLabel(self.groupBox)
        self.label_path.setGeometry(QtCore.QRect(10, 80, 301, 20))
        self.label_path.setAlignment(QtCore.Qt.AlignCenter)
        self.label_path.setObjectName(_fromUtf8("label_path"))
        self.groupBox_2 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(0, 140, 321, 141))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.cb_getsheetname = QtGui.QComboBox(self.groupBox_2)
        self.cb_getsheetname.setGeometry(QtCore.QRect(60, 60, 201, 22))
        self.cb_getsheetname.setObjectName(_fromUtf8("cb_getsheetname"))
        #MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 328, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        #MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        #MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Datagouv geocoder v1.0", None))
        self.groupBox_4.setTitle(_translate("MainWindow", "Step 3 : Select addr column header", None))
        self.groupBox_3.setTitle(_translate("MainWindow", "Step 4 : Get results", None))
        self.label_results.setText(_translate("MainWindow", "No results", None))
        self.groupBox.setTitle(_translate("MainWindow", "Step 1 : Get source file", None))
        self.btn_getpath.setText(_translate("MainWindow", "Find your file", None))
        self.label_path.setText(_translate("MainWindow", "No file selected", None))
        self.groupBox_2.setTitle(_translate("MainWindow", "Step 2 : Select sheet name", None))
