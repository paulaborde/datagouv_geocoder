# coding: utf8
###############################################################
# Library to code address in lat, long with Data.gouv.fr      #
# paul.laborde@openmailbox.org                                #
###############################################################

# libraries
import requests
import pandas as pd
import sys
import xlrd

def getsheets(myfile):
    xl = xlrd.open_workbook(myfile, on_demand=True)
    return(xl.sheet_names())

def checkheaders(myfile, mysheet):
    tempdf = pd.read_excel(myfile, sheetname=mysheet)
    return (tempdf)

def geocode_df(mydf, myheader):
    final_df = mydf.apply(lambda x: geocode(x, myheader), axis=1)
    return(final_df)

def geocode(x, header):
    url = 'https://api-adresse.data.gouv.fr/search/?q=' + x[header]
    r = requests.get(url)
    jresultat = r.json()

    temp_dict = {'address':x[header], 'lat':'0', 'long':'0'}

    # Test if no geometry data found
    if jresultat['features']!=[]:
        if 'geometry' in jresultat['features'][0].keys():
            temp_dict = {'address':x[header], 'lat':jresultat['features'][0]['geometry']['coordinates'][0], 'long':jresultat['features'][0]['geometry']['coordinates'][1]}

    return(pd.Series(temp_dict))
